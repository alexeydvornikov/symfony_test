<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 01.10.2019
 * Time: 20:58
 */

namespace App\Command;

use App\Service\EmployeeManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EmployeeCanCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('employee:can')
            ->setDescription('Displays is it possible to do this skill')
            ->addArgument('position', InputArgument::REQUIRED, 'Employee position')
            ->addArgument('action', InputArgument::REQUIRED, 'Employee action')
            ->setHelp('This command displays an employee’s ability to do something');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $employeeManager = new EmployeeManager();
        $result = $employeeManager->canDoThisAction(
            $input->getArgument('position'),
            $input->getArgument('action')
        );
        $output->writeln($result);
    }
}

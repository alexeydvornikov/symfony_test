<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 01.10.2019
 * Time: 20:58
 */

namespace App\Command;

use App\Service\EmployeeManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CompanyEmployeeCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('company:employee')
            ->addArgument('position', InputArgument::REQUIRED, 'Employee position')
            ->setDescription('Displays employee skills')
            ->setHelp('This command displays a list of employee skills.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $employeeManager = new EmployeeManager();
        $position = $input->getArgument('position');
        $skills = $employeeManager->getEmployeeSkills($position);
        $output->writeln([
            'A list of ' . $position . ' skills',
            '========================='
        ]);
        $output->writeln($skills);

    }
}
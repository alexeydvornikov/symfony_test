<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 01.10.2019
 * Time: 23:40
 */

namespace App\Entity\Employee;


class Manager extends Employee implements SetTasksInterface
{
    public function setTasks(): string
    {
        return 'task setting';
    }

}

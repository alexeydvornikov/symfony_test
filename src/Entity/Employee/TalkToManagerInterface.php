<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 01.10.2019
 * Time: 23:56
 */

namespace App\Entity\Employee;


interface TalkToManagerInterface
{
    public function talkToManager();
}

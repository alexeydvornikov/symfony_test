<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 01.10.2019
 * Time: 23:43
 */

namespace App\Entity\Employee;


class Tester extends Employee implements
    TestCodeInterface,
    SetTasksInterface,
    TalkToManagerInterface
{
    public function talkToManager(): string
    {
        return 'communication with manager';
    }

    public function setTasks(): string
    {
        return 'task setting';
    }

    public function testCode(): string
    {
        return 'testing code';
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 01.10.2019
 * Time: 23:40
 */

namespace App\Entity\Employee;


class Designer extends Employee implements TalkToManagerInterface
{
    public function talkToManager(): string
    {
        return 'communication with manager';
    }

    public function draw(): string
    {
        return 'drawing';
    }
}

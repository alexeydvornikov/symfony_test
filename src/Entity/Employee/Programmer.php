<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 01.10.2019
 * Time: 23:38
 */

namespace App\Entity\Employee;


class Programmer extends Employee implements
    TalkToManagerInterface,
    TestCodeInterface
{
    public function talkToManager(): string
    {
        return 'communication with manager';
    }

    public function writeCode(): string
    {
        return 'code writing';
    }

    public function testCode(): string
    {
        return 'code testing';
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 01.10.2019
 * Time: 22:17
 */

namespace App\Service;


class EmployeeManager
{

    public function getEmployeeSkills($position)
    {
        $class = 'App\Entity\Employee\\' . ucfirst($position);
        if (class_exists($class)) {
            try {
                $employee = new $class;
            } catch (\Throwable $e) {
                return 'No such employee';
            }
            $skills = get_class_methods($employee);
            foreach ($skills as $skill) {
                $res[] = $employee->$skill();
            }
            return $res;
        }
        return 'No such employee';
    }

    public function canDoThisAction($position, $action)
    {
        $class = 'App\Entity\Employee\\' . ucfirst($position);
        if (class_exists($class)) {
            try {
                $employee = new $class;
            } catch (\Throwable $e) {
                return 'No such employee';
            }
            $actions = get_class_methods($employee);
            return (in_array($action, $actions)) ? 'true' : 'false';
        }
        return 'No such employee';
    }

}
